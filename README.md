### SSH 複数の、公開鍵の登録
---
```
ssh-keygen -t rsa
vi ~/.ssh/id_rsa.pub
PASTE // ssh key
ssh -t git@github@github.com
```

```
ssh-keygen -f ~/.ssh/id_rsa_github.pub
/*
vi ~/.ssh/id_rsa.pub
Ctrl + C
*/
pbcopy < ~/.ssh/id_rsa.pub
/*
SSH and GPG keys on github
*/
ssh -T git@git@github.com

CREATE APPTKY6

echo "### apptky6" >> README.md
git init .
git add .
git commit -m "1st"
git remote add origin git@github.com:takagotch/apptky6.git
git push -u origin master
```


```sh
// ssh-keygen -f ~/.ssh/id_rsa.pub
ssh-keygen -f ~/.ssh/id_rsa_github.pub
ssh-keygen -f ~/.ssh/id_rsa_bitbucket.pub

ls -al 

vi ~/.ssh/config
/*
HostName 168.192.xx.xx
User vagrant
Port 2222
UserKnownHostFile /dev/null
StrictHostKeyChecking no
PasswordAuthentication no
IdentityFile C:/Users/tky/.vagrant.d/insecure_private_key
IdentitiesOnly yes
LogLevel FATAL
*/
Host github.com
  User git 
  HostName github.com
  IdentityFile ~/.ssh/id_rsa_github
  IdentitiesOnly yes 

Host bitbucket.org
  User git 
  HostName bitbucket.org
  IdentityFile ~/.ssh/id_rsa_bitbucket
  IdentitiesOnly yes
  
cat ~/.ssh/id_rsa_github.pub | pbcopy


ssh -T git@github.com
ssh -T git@bitbucket.org

CREATE PROJECT/REPOSITORY
echo "### SSH-KEY-SETTINGS" >> README.md
git add .
git commit -m "1st commit "
git push -u origin master
```

```yml
Title: id_rsa_bitbucket
Key:   ssh-rsa AAAABxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

```
// known_hosts
bitbucket.org,xx.xxx.xxx.x ssh-rs AAAABxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
xx.xxx.xx.x ssh-rsa AAAABxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

